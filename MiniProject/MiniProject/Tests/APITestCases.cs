﻿using MiniProject.Controllers;
using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;

namespace MiniProject.Tests
{
	[TestFixture]
	public class APITestCases
	{
		ValuesController v=new ValuesController();
		List<Exp> actual = new List<Exp>()
		{
			new Exp{ID=0,ExpenseType= "supplies",ExpenseAmount=120,Description= "asdf",Receipt=1,ResidenceID= 12}
		};
		[Test]
		public void Test1()
		{
			//List<Exp> li = v.Get();
			List<Exp> l = v.Get(0);
			Assert.AreEqual(l,actual);
		}
	}
}
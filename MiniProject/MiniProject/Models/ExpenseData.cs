﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MiniProject.Models
{
    public class ExpenseData
    {
		string conn;
		SqlCommand _command;
		public ExpenseData()
		{
			conn = ConfigurationManager.ConnectionStrings["Project"].ToString();
		}

		public List<Expenses> GetAllData()
        {
            List<Expenses> PList = new List<Expenses>();
            using (SqlConnection con = new SqlConnection(conn))
            {
                SqlCommand cmd = new SqlCommand("sp_GetData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Expenses package = new Expenses();
                    package.ID = Convert.ToInt32(dr["id"]);
					package.ExpenseDate = (DateTime)dr["ExpenseDate"];
					package.ExpenseType = dr["ExpenseType"].ToString();
					package.ExpenseAmount = Convert.ToInt32((dr["ExpenseAmount"]));
					package.Description = dr["Description"].ToString();
					package.Receipt = Convert.ToByte(dr["Receipt"]);
                    package.ResidenceID = Convert.ToInt32(dr["ResidenceID"]);
                    
                    PList.Add(package);
                }
                con.Close();
            }
            return PList;
        }
        public void AddData(Expenses p)
        {
            int ids = 0;
            using (SqlConnection _con = new SqlConnection(conn))
            {
                _command = _con.CreateCommand();
                _command.CommandType = CommandType.StoredProcedure;
                _command.CommandText = "sp_InsertData";
                _command.Parameters.AddWithValue("@ID", p.ID);
				_command.Parameters.AddWithValue("@ExpenseDate", p.ExpenseDate);
				_command.Parameters.AddWithValue("@ExpenseType", p.ExpenseType);
				_command.Parameters.AddWithValue("@ExpenseAmount", p.ExpenseAmount);
				_command.Parameters.AddWithValue("@Description", p.Description);
				_command.Parameters.AddWithValue("@Receipt", p.Receipt);
				_command.Parameters.AddWithValue("@ResidenceID", p.ResidenceID);
				_con.Open();
                ids = _command.ExecuteNonQuery();
                _con.Close();
            }
        }

        public void UpdateData(Expenses p)
        {
            int ids = 0;
            using (SqlConnection _con = new SqlConnection(conn))
            {
                _command = _con.CreateCommand();
                _command.CommandType = CommandType.StoredProcedure;
                _command.CommandText = "sp_UpdateData";
				_command.Parameters.AddWithValue("@ID", p.ID);
				_command.Parameters.AddWithValue("@ExpenseDate", p.ExpenseDate);
				_command.Parameters.AddWithValue("@ExpenseType", p.ExpenseType);
				_command.Parameters.AddWithValue("@ExpenseAmount", p.ExpenseAmount);
				_command.Parameters.AddWithValue("@Description", p.Description);
				_command.Parameters.AddWithValue("@Receipt", p.Receipt);
				_command.Parameters.AddWithValue("@ResidenceID", p.ResidenceID);
				_con.Open();
                ids = _command.ExecuteNonQuery();
                _con.Close();
            }
        }

        public Expenses GetExpenseData(int id)
        {
            List<Expenses> PList1 = new List<Expenses>();
			Expenses package = new Expenses();
            using (SqlConnection _con = new SqlConnection(conn))
            {
                string sqlQuery = "SELECT * FROM ExpenseTracking WHERE id= " + id;
                SqlCommand cmd = new SqlCommand(sqlQuery, _con);
                _con.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
					package.ID = Convert.ToInt32(dr["id"]);
					package.ExpenseDate = (DateTime)dr["ExpenseDate"];
					package.ExpenseType = dr["ExpenseType"].ToString();
					package.ExpenseAmount = Convert.ToInt32(dr["ExpenseAmount"]);
					package.Description = dr["Description"].ToString();
					package.Receipt = Convert.ToByte(dr["Receipt"]);
					package.ResidenceID = Convert.ToInt32(dr["ResidenceID"]);
					PList1.Add(package);
                }
                _con.Close();
            }
            return package;
        }

        public void DeleteData(int id)
        {
            using (SqlConnection con = new SqlConnection(conn))
            {
                SqlCommand cmd = new SqlCommand("sp_DeleteData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id",id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
    }
}

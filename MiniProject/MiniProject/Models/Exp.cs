﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MiniProject.Models
{
	public class Exp
	{
		[Required]
		public int ID { get; set; }
		[Required]
		public string ExpenseType { get; set; }
		[Required]
		public int ExpenseAmount { get; set; }
		[Required]
		public string Description { get; set; }
		[Required]
		public byte Receipt { get; set; }
		[Required]
		public int ResidenceID { get; set; }
	}
}
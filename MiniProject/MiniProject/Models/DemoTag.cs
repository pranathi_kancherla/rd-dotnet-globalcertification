﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MiniProject.Models
{
	public class DemoTag:DbContext
	{
		public DemoTag() : base("Project")
		{

		}
		public DbSet<Tags> Demo { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiniProject.Models
{
	public class Tags
	{
		public int TagID { get; set; }
		public string TagName { get; set; }
		public string Description { get; set; }
		
	}
}
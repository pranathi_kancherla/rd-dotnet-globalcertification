﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace MiniProject.Models
{
	public class DataAccess1
	{
		public string conn;
		SqlCommand _command;
		public DataAccess1()
		{
			conn = ConfigurationManager.ConnectionStrings["Project"].ToString();
		}
		public int InsertData(SignUpModel model)
		{
			int ids;
			using (SqlConnection con = new SqlConnection(conn))
			{
				_command = con.CreateCommand();
				_command.CommandType = CommandType.StoredProcedure;
				_command.CommandText = "sp_InsertData";
				_command.Parameters.AddWithValue("@username", model.Username);
				_command.Parameters.AddWithValue("@password", model.Password);
				_command.Parameters.AddWithValue("@Email", model.Email);
				_command.Parameters.AddWithValue("@FullName", model.FullName);
				con.Open();
				ids = _command.ExecuteNonQuery();
				con.Close();
			}
			return ids;
		}
	}
}
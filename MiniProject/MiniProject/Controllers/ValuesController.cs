﻿using MiniProject.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Xml.Linq;

namespace MiniProject.Controllers
{
    [EnableCors("*","*","*")]
    public class ValuesController : ApiController
    {
		ExpenseData _data = new ExpenseData();
		public List<Expenses> Get()
		{
			List<Expenses> list = new List<Expenses>();
			list = _data.GetAllData();
			return list;
		}

		// GET api/values/5
		public List<Exp> Get(int id)
		{
			List<Exp> l1 = new List<Exp>()
			{
				new Exp{ID=0,ExpenseType= "supplies",ExpenseAmount=120,Description= "asdf",Receipt=1,ResidenceID= 12}
			};
			return l1;
		}

		// POST api/values
		public void Post([FromBody] string value)
		{
		}

		// PUT api/values/5
		public void Put(int id, [FromBody] string value)
		{
		}

		// DELETE api/values/5
		public void Delete(int id)
		{
		}
	}
}

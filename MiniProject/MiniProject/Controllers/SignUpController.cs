﻿using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiniProject.Controllers
{
    public class SignUpController : Controller
    {
		DataAccess1 d = new DataAccess1();
		[HttpGet]
		public ActionResult Create()
		{
			return View();
		}
		[HttpPost]
		public ActionResult Create(SignUpModel data)
		{
			int id = d.InsertData(data);
			if (id == 1)
			{
				return RedirectToRoute("Home/Index");
			}
			else
			{
				return View();
			}
		}

		// POST: SignUp/SignUp/Create
		
	}
}
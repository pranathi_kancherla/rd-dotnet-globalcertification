﻿
using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiniProject.Controllers
{
    public class LoginController : Controller
    {
		DataAccess d1= new DataAccess();
		// GET: Login
		[HttpGet]
		public ActionResult Create()
		{
			return View();
		}

		// POST: Login/Login/Create
		[HttpPost]
		public ActionResult Create(LoginModel model)
		{
			int id = d1.CheckForm(model);
			if (id != 0)
			{
				return RedirectToAction("~/Home/Contact");
			}
			else
			{
				return View(model);
			}
		}
	}
}
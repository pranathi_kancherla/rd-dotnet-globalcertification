﻿using Microsoft.AspNetCore.Http;
using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiniProject.Controllers
{
	public class ExpensesController : Controller
	{
		ExpenseData _data=new ExpenseData();
		public ActionResult Index()
		{
			List<Expenses> list = new List<Expenses>();
			list = _data.GetAllData();
			return View(list);
		}

		// GET: PackageController/Details/5
		public ActionResult Details(int id)
		{
			Expenses p = _data.GetExpenseData(id);
			return View(p);
		}

		// GET: PackageController/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: PackageController/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(Expenses p)
		{
			try
			{
				_data.AddData(p);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}
		[HttpGet]
		// GET: PackageController/Edit/5
		public ActionResult Edit(int id)
		{
			Expenses p = _data.GetExpenseData(id);
			return View(p);
		}

		// POST: PackageController/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(Expenses p)
		{
			try
			{
				_data.UpdateData(p);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return RedirectToAction(nameof(Index));
			}
		}

		// GET: PackageController/Delete/5
		[HttpGet]
		public ActionResult Delete(int id)
		{
			Expenses p = _data.GetExpenseData(id);
			return View(p);
		}

		// POST: PackageController/Delete/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id, IFormCollection collection)
		{
			try
			{
				_data.DeleteData(id);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}
		public ActionResult Sample()
		{
			List<Expenses> list = new List<Expenses>();
			list = _data.GetAllData();
			return View(list);
		}
	}
}
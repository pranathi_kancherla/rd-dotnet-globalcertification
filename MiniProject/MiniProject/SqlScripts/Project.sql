create database project
use project
create table ApartmentResidents(ResidentId int primary key,FirstName varchar(30) not null,LastName varchar(30) not null,FlatNumber int not null,OwnershipType varchar(20) not null,ContactNumber bigint not null,Email varchar(50) not null,MoveInDate Date not null)
create table MonthlyMaintenance(ID int identity,ResidenceID int not null,Month varchar(20) not null,Year int not null,MaintenanceAmount decimal(10,2) not null,PaymentDate date not null,PaymentStatus varchar(20) not null,PaymentMethodID int not null,CategoryID int,Notes varchar(100) primary key(ResidenceID,CategoryID))
create table PaymentMethods(PaymentMethodID int primary key,PaymentMethodName varchar(50) not null,Description varchar(100) not null,IsActive varchar not null)
create table ExpenseTracking(ID int primary key,ExpenseDate date not null,ExpenseType varchar(50) not null,ExpenseAmount decimal(10,2),Description varchar(100),Receipt bit not null,ResidenceID int not null)
create table Users(UserID int primary key identity,Username varchar(50),Password varchar(100) not null,Email varchar(100),FullName varchar(100),CreatedAt datetime,UpdatedAt datetime)
create table Tags(TagID int primary key not null,TagName varchar(50) not null,Description varchar(100) not null)
create table ExpenseTags(ExpenseTagID int,ExpenseID int,TagID int,foreign key(TagID) references Tags(TagID))
create table ExpenseCategories(ExpenseCategoryID int,CategoryID int,foreign key(CategoryID) references MonthlyMaintenance(CategoryID))
create proc sp_InsertData
(
	@username varchar(50),
	@password varchar(100),
	@Email varchar(100),
	@FullName varchar(100)
)
as
begin
	insert into Users (Username,Password,Email,FullName,CreatedAt,UpdatedAt) 
	values (@username,@password,@Email,@FullName,GETDATE(),GETDATE())
end

-- stored procedures for expenses tracking table
create proc sp_GetData
as
begin
	select * from ExpenseTracking
end

create proc sp_InsertData
(
	@ID int,
	@ExpenseDate date,
	@ExpenseType varchar(50),
	@ExpenseAmount int,
	@Description varchar(100),
	@Receipt bit,
	@ResidenceID int
)
as
begin
	insert into ExpenseTracking(ID,ExpenseDate,ExpenseType,ExpenseAmount,Description,Receipt,ResidenceID)
	values (@ID,@ExpenseDate,@ExpenseType,@ExpenseAmount,@Description,@Receipt,@ResidenceID)
end

create proc sp_UpdateData
(
	@ID int,
	@ExpenseDate date,
	@ExpenseType varchar(50),
	@ExpenseAmount int,
	@Description varchar(100),
	@Receipt bit,
	@ResidenceID int
)
as
begin
	declare @RowCount int=0
	begin try
		set @RowCount=(select count(1) from ExpenseTracking where ID=@ID)
		if(@RowCount>0)
		begin
			begin tran
				update ExpenseTracking set ID=@ID,ExpenseDate=@ExpenseDate,ExpenseType=@ExpenseType,ExpenseAmount=@ExpenseAmount,Description=@Description,Receipt=@Receipt,ResidenceID=@ResidenceID where ID=@ID
			commit tran
		end
	end try
begin catch
	Rollback tran
end catch
end

create proc sp_DeleteData
(@id int)
as
begin
declare @RowCount int = 0
begin try
	set @RowCount=(select count(1) from ExpenseTracking where id=@id)
	if(@RowCount>0)
	begin
		begin tran
			delete from ExpenseTracking where id=@id
		commit tran
	end
end try
begin catch
	Rollback tran
end catch
end

	
